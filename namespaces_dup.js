var namespaces_dup =
[
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "HW_0x01", "namespaceHW__0x01.html", [
      [ "getChange", "namespaceHW__0x01.html#abfa185fb49bdf353e198ad02ab389654", null ],
      [ "cng", "namespaceHW__0x01.html#a868cbf54029436fb6e6d4b6aa8146051", null ],
      [ "cngVal", "namespaceHW__0x01.html#a4740893c3a074a5bd64df64e0d401f45", null ],
      [ "denoms", "namespaceHW__0x01.html#a133ac2b808698b3316db8c9211667639", null ],
      [ "pmt", "namespaceHW__0x01.html#a4dbb0a8dbed6fc8408ff541a2b051e8e", null ],
      [ "pmtVal", "namespaceHW__0x01.html#ad638f997e0a837494e540d388d0276ab", null ],
      [ "price", "namespaceHW__0x01.html#a6ffa02ed681f39b50cd02eb097c3f254", null ],
      [ "test_cases", "namespaceHW__0x01.html#a73f90f8633cce5f280ebeb6c31b8abf8", null ]
    ] ],
    [ "Lab0x01", null, [
      [ "onButtonPressFCN", "Lab0x01_8py.html#a30a5cf8034929397cada3568b302d96a", null ],
      [ "reset_timer", "Lab0x01_8py.html#ac60cf12ea29eb4c53b0a55d65db78cf0", null ],
      [ "UPDATE_SIW", "Lab0x01_8py.html#a244d64edf563ffa30879200a22d7fe92", null ],
      [ "UPDATE_SQW", "Lab0x01_8py.html#ac89afbf25e21e25ff35af715ceb7644c", null ],
      [ "UPDATE_STW", "Lab0x01_8py.html#ac1097cb82e54d5ac4c1c3715254600c7", null ],
      [ "update_timer", "Lab0x01_8py.html#a2411427ca814bad0117683ddc2488c06", null ],
      [ "pinC13", "Lab0x01_8py.html#ab7350683e4e73629aaa40f90ce9b45d9", null ],
      [ "state", "Lab0x01_8py.html#af49dfbadfaec711466de0f806232bf66", null ],
      [ "tim2", "Lab0x01_8py.html#a778761870b1d6720e614c945eaa72b9f", null ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ]
];