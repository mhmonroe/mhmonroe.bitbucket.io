var files_dup =
[
    [ "305_HW0x02_Page.py", "305__HW0x02__Page_8py.html", null ],
    [ "305_HW0x03_Page.py", "305__HW0x03__Page_8py.html", null ],
    [ "305_Lab0x01_Page.py", "305__Lab0x01__Page_8py.html", null ],
    [ "305_Lab0x02_Page.py", "305__Lab0x02__Page_8py.html", null ],
    [ "305_Lab0x03_Page.py", "305__Lab0x03__Page_8py.html", null ],
    [ "305_Lab0x05_Page.py", "305__Lab0x05__Page_8py.html", null ],
    [ "305_Term_Project_Page.py", "305__Term__Project__Page_8py.html", null ],
    [ "closedloop.py", "closedloop_8py.html", [
      [ "closedloop.ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "driver.py", "driver_8py.html", [
      [ "driver.DRV8847", "classdriver_1_1DRV8847.html", "classdriver_1_1DRV8847" ],
      [ "driver.Motor", "classdriver_1_1Motor.html", "classdriver_1_1Motor" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "imu.py", "imu_8py.html", [
      [ "imu.BNO055", "classimu_1_1BNO055.html", "classimu_1_1BNO055" ]
    ] ],
    [ "Lab0x01.py", "Lab0x01_8py.html", "Lab0x01_8py" ],
    [ "main.py", "main_8py.html", null ],
    [ "main_lab0x04.py", "main__lab0x04_8py.html", null ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_calibration.py", "task__calibration_8py.html", [
      [ "task_calibration.Task_Calibration", "classtask__calibration_1_1Task__Calibration.html", "classtask__calibration_1_1Task__Calibration" ]
    ] ],
    [ "task_controller.py", "task__controller_8py.html", [
      [ "task_controller.Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_driver.py", "task__driver_8py.html", [
      [ "task_driver.Task_Driver", "classtask__driver_1_1Task__Driver.html", "classtask__driver_1_1Task__Driver" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", [
      [ "task_encoder.Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_imu.py", "task__imu_8py.html", [
      [ "task_imu.Task_IMU", "classtask__imu_1_1Task__IMU.html", "classtask__imu_1_1Task__IMU" ]
    ] ],
    [ "task_panel.py", "task__panel_8py.html", [
      [ "task_panel.Task_Panel", "classtask__panel_1_1Task__Panel.html", "classtask__panel_1_1Task__Panel" ]
    ] ],
    [ "task_user.py", "task__user_8py.html", [
      [ "task_user.Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ],
    [ "task_user_lab0x04.py", "task__user__lab0x04_8py.html", [
      [ "task_user_lab0x04.Task_User", "classtask__user__lab0x04_1_1Task__User.html", "classtask__user__lab0x04_1_1Task__User" ]
    ] ],
    [ "touchpanel.py", "touchpanel_8py.html", [
      [ "touchpanel.Panel", "classtouchpanel_1_1Panel.html", "classtouchpanel_1_1Panel" ]
    ] ]
];